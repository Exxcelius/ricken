package de.thb.ricken.datatransfer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.thb.ricken.entity.UserEntity;
import de.thb.ricken.entity.WorkoutEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WorkoutDto {

	public WorkoutDto(WorkoutEntity workout) {
		setName(workout.getName());
		setDescription(workout.getDescription());
		setUser(workout.getUser());
		setId(workout.getId());
	}

	@NotNull
	@NotEmpty
	//@Size(max=80)
	private String name;
	
	@NotNull
	private String description;
	
	private UserEntity user;
	
	private long id;
}
