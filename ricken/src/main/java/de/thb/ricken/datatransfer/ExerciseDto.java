package de.thb.ricken.datatransfer;

import de.thb.ricken.entity.ExerciseEntity;
import de.thb.ricken.entity.WorkoutEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExerciseDto {

	public ExerciseDto(ExerciseEntity exercise) {
		setId(exercise.getId());
		setWorkout(exercise.getWorkout());
		
		setName(exercise.getName());
		setDescription(exercise.getDescription());
	}
	
	private String name;
	private String Description;
	private long id;
	private WorkoutEntity workout;

}
