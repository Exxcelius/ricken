package de.thb.ricken.datatransfer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import de.thb.ricken.validation.PasswordMatches;
import de.thb.ricken.validation.ValidEmail;
import lombok.Data;

@Data
@PasswordMatches
public class UserDto {
	
	@NotNull
	@NotEmpty
	private String username;
	
	@ValidEmail
	@NotNull
	@NotEmpty
	private String email;
	
	@NotNull
	@NotEmpty
	private String password;
	private String matchingPassword;

}
