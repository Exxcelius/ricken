package de.thb.ricken.controller.web;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.codehaus.groovy.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import de.thb.ricken.entity.ExerciseEntity;
import de.thb.ricken.entity.UserEntity;
import de.thb.ricken.entity.WorkoutEntity;
import de.thb.ricken.repository.ExerciseRepository;
import de.thb.ricken.repository.UserRepository;
import de.thb.ricken.repository.WorkoutRepository;
import de.thb.ricken.datatransfer.ExerciseDto;
import de.thb.ricken.datatransfer.UserDto;
import de.thb.ricken.datatransfer.WorkoutDto;
import de.thb.ricken.service.ExerciseFeatureUnitService;
import de.thb.ricken.service.UserAlreadyExistException;
import de.thb.ricken.service.UserService;
import de.thb.ricken.service.WorkoutService;
import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class ExampleController {

	private final UserRepository userRepository;
	private final WorkoutRepository workoutRepository;
	private final ExerciseRepository exerciseRepository;
	
	@Autowired
	private final UserService userService;
	
	@Autowired
	private final WorkoutService workoutService;
	
	@Autowired
	private final ExerciseFeatureUnitService unitService;
	
	private Optional<UserEntity> findByEmailOrUsername(String cred) {
		Optional<UserEntity> userDetailsOptional = userRepository.findByEmail(cred);
		if (userDetailsOptional.isEmpty()) {
			userDetailsOptional = userRepository.findByUsername(cred);
		}
		return userDetailsOptional;
	}

	@GetMapping("/")
	public String showNotebooks(Model model) {
		return "home";
	}

	@GetMapping("secure")
	public String securedPage() {
		return "secure";
	}

	@GetMapping("dashboard")
	public String profile(Model model, Principal principal) {
		UserEntity user = findByEmailOrUsername(principal.getName()).get();

		model.addAttribute("workouts", user.getWorkouts());
		return "profile";
	}

	@GetMapping("sign-up")
	public String signUp(WebRequest request, Model model) {
		UserDto userDto = new UserDto();
		model.addAttribute("user", userDto);
		return "sign-up";
	}

	@PostMapping("sign-up")
	public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto, HttpServletRequest request,
			Errors errors, ModelAndView mav) {

		try {
			UserEntity registered = userService.registerNewUserAccount(userDto);
		} catch (UserAlreadyExistException uaeEx) {
			mav.addObject("message", "An account for that username/email already exists.");
			return mav;
		}

		return new ModelAndView("successRegister", "user", userDto);
	}
	
	@GetMapping("workout/create")
	public String createWorkout(Model model) {
		System.out.println("Reached Get mapping");
		WorkoutDto workoutDto = new WorkoutDto();
		model.addAttribute("workoutDto", workoutDto);
		model.addAttribute("action", "/workout/create");
		return "createWorkout";
	}
	
	@PostMapping("workout/create")
	public ModelAndView createWorkout(@ModelAttribute("workoutDto") @Valid WorkoutDto workoutDto, HttpServletRequest request,
			Errors error, ModelAndView mav, ModelMap model, Principal principal){
		System.out.println("Reached Mapping");
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		workoutDto.setUser(user);
		
		WorkoutEntity created;
		try {
			created = workoutService.createNewWorkout(workoutDto);
		}
		catch(Exception e) {
			mav.addObject("message", "An error occured");
			return mav;
		}
		return new ModelAndView("redirect:/workout/" + created.getId());
	}
	
	@GetMapping("workout/{id}/edit")
	public String editWorkout(@PathVariable("id") long id, Model model, Principal principal) {
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		WorkoutEntity workout = workoutRepository.findById(id).get();
		WorkoutDto workoutDto= new WorkoutDto(workout);
		
		if (user.getWorkouts().contains(workout)) {
			model.addAttribute("workoutDto", workoutDto);
			model.addAttribute("action", "/workout/" + id + "/edit");
			return "createWorkout";
		}
		else throw new AccessDeniedException("403 returned");
	}
	
	@PostMapping("workout/{id}/edit")
	public ModelAndView editWorkout(@ModelAttribute("workoutDto") @Valid WorkoutDto workoutDto, @PathVariable("id") long id,
			ModelAndView mav, ModelMap model, Principal principal) {
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		WorkoutEntity workout = workoutRepository.findById(id).get();
		
		if (user.getWorkouts().contains(workout)) {
			try {
				workout = workoutService.updateWorkout(workoutDto);
			}
			catch(Exception e) {
				mav.addObject("message", "An error occured");
				return mav;
			}
			return new ModelAndView("redirect:/workout/" + workout.getId());
		}
		else throw new AccessDeniedException("403 returned");
	}
	
	@GetMapping("workout/{id}")
	public String workoutDetails(@PathVariable("id") long id, Model model, Principal principal) {
		System.out.println("" + id);
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		WorkoutEntity workout = workoutRepository.findById(id).get();
		
		if (user.getWorkouts().contains(workout)) {
			model.addAttribute("workout", workout);
			return "workoutDetails";
		}
		else throw new AccessDeniedException("403 returned");
	}
	
	@GetMapping("/workout/{id}/exercise/create")
	public String createExercise(@PathVariable("id") long workoutId, Model model, Principal principal) {
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		WorkoutEntity workout = workoutRepository.findById(workoutId).get();
		ExerciseDto exerciseDto = new ExerciseDto();
		if (user.getWorkouts().contains(workout)) {
			model.addAttribute("action", workoutId);
			model.addAttribute("allUnits", unitService.findAll());
			model.addAttribute("exerciseDto", exerciseDto);
			return "createExercise";
		}
		else throw new AccessDeniedException("403 returned");
	}
	
	@PostMapping("/workout/{id}/exercise/create")
	public ModelAndView createExercisePost(@PathVariable("id") long workoutId, Model model, Principal principal) {
		System.out.println("Posted exercise");
		return null;
	}
	
	@GetMapping("/workout/exercise/{exId}/edit")
	public String editExercise(@PathVariable("exId") long exerciseId, Model model, Principal principal) {
		ExerciseEntity exercise = exerciseRepository.findById(exerciseId).get();
		WorkoutEntity workout = workoutRepository.findById(exercise.getWorkout().getId()).get();
		UserEntity user = findByEmailOrUsername(principal.getName()).get();
		
		if (!user.getWorkouts().contains(workout)) {
			throw new AccessDeniedException("403 returned");
		}
		
		model.addAttribute("exercise", new ExerciseDto(exercise));
		return "createExercise";
		
	}
	

		
}
