package de.thb.ricken.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name="ExerciseFeatureUnit")
public class ExerciseFeatureUnitEntity {
	@Id
	private long id;
	private String name;
}
