package de.thb.ricken.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.thb.ricken.entity.ExerciseFeatureUnitEntity;
import de.thb.ricken.repository.ExerciseFeatureUnitRepository;

@Service
public class ExerciseFeatureUnitService implements IExerciseFeatureUnitService {
	@Autowired
	private ExerciseFeatureUnitRepository repo;

	@Override
	public List<ExerciseFeatureUnitEntity> findAll() {
		List<ExerciseFeatureUnitEntity> out = new ArrayList<ExerciseFeatureUnitEntity>();
		repo.findAll().forEach(e -> {out.add(e);});
		return out;
	}

	@Override
	public ExerciseFeatureUnitEntity findById(long id) {
		return repo.findById(id).get();
	}

}
