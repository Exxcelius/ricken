package de.thb.ricken.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.thb.ricken.datatransfer.WorkoutDto;
import de.thb.ricken.entity.WorkoutEntity;
import de.thb.ricken.repository.WorkoutRepository;

@Service
public class WorkoutService implements IWorkoutService{

	@Autowired
	private WorkoutRepository rep;
	
	@Override
	@Transactional
	public WorkoutEntity createNewWorkout(WorkoutDto workoutDto){
		System.out.println("Reached WorkoutService");
		WorkoutEntity workout = new WorkoutEntity();
		workout.setName(workoutDto.getName());
		workout.setDescription(workoutDto.getDescription() == null ? "" : workoutDto.getDescription());
		workout.setUser(workoutDto.getUser());
		
		// for debug purposes; rep.save can change entity
		workout = rep.save(workout);
		return workout;
	}

	public WorkoutEntity updateWorkout(WorkoutDto workoutDto) {
		WorkoutEntity workout = rep.findById(workoutDto.getId()).get();
		workout.setName(workoutDto.getName());
		workout.setDescription(workoutDto.getDescription());
		
		
		workout = rep.save(workout);
		return workout;
	}

}
