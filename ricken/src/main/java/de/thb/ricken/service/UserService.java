package de.thb.ricken.service;

import java.time.LocalDateTime;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.thb.ricken.entity.UserEntity;
import de.thb.ricken.repository.UserRepository;
import de.thb.ricken.datatransfer.UserDto;

@Service
public class UserService implements IUserService{

	@Autowired
	private UserRepository rep;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	@Transactional
	public UserEntity registerNewUserAccount(@Valid UserDto userDto) throws UserAlreadyExistException {
		if (emailExist(userDto.getEmail())) throw new UserAlreadyExistException("There is an account with that email address: " + userDto.getEmail());
		if (usernameExist(userDto.getUsername())) throw new UserAlreadyExistException("There is an account with that username: " + userDto.getUsername());
		
		UserEntity user = new UserEntity();
		user.setUsername(userDto.getUsername());
		user.setEmail(userDto.getEmail());
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		user.setEnabled(true);
		user.setDate_created(LocalDateTime.now());
		
		// for debug purposes; rep.save can change user
		user = rep.save(user);
		return user;
	}
	
	private boolean emailExist(String email) {
		return rep.findByEmail(email).isPresent();
	}
	
	private boolean usernameExist(String username) {
		return rep.findByUsername(username).isPresent();
	}

}
