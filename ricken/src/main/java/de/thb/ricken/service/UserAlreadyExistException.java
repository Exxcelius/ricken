package de.thb.ricken.service;

public class UserAlreadyExistException extends Exception {

	public UserAlreadyExistException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
