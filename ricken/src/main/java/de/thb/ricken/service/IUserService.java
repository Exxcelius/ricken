package de.thb.ricken.service;

import de.thb.ricken.entity.UserEntity;
import de.thb.ricken.datatransfer.UserDto;

public interface IUserService {
	public UserEntity registerNewUserAccount(UserDto userDto) throws UserAlreadyExistException;
}
