package de.thb.ricken.service;

import de.thb.ricken.datatransfer.WorkoutDto;
import de.thb.ricken.entity.WorkoutEntity;

public interface IWorkoutService {
	public WorkoutEntity createNewWorkout(WorkoutDto workout);
}
