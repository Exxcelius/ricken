package de.thb.ricken.service;

import java.util.List;

import org.springframework.stereotype.Service;
import de.thb.ricken.entity.ExerciseFeatureUnitEntity;

public interface IExerciseFeatureUnitService {
	
	public List<ExerciseFeatureUnitEntity> findAll();
	public ExerciseFeatureUnitEntity findById(long id);
}
