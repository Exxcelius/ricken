package de.thb.ricken.security;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import de.thb.ricken.entity.UserEntity;
import de.thb.ricken.repository.UserRepository;
import de.thb.ricken.security.authority.UserAuthority;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
	
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	
    	Optional<UserEntity> userDetailsOptional = userRepository.findByEmail(username);
    	if (userDetailsOptional.isEmpty()) {
    		userDetailsOptional = userRepository.findByUsername(username);
    	}

    	UserDetailsImpl eud = userDetailsOptional.map(user -> {
    				return UserDetailsImpl.builder()
    						.username(user.getEmail())
    	                    .password(user.getPassword())
    	                    .authorities(List.of(new UserAuthority()))
    	                    .enabled(user.isEnabled())
    	                    .accountNonExpired(true)
    	                    .accountNonLocked(true)
    	                    .credentialsNonExpired(true)
    						.build();
    			})
    			.orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    	
    	return eud;
    }
}
