package de.thb.ricken.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;

import de.thb.ricken.entity.WorkoutEntity;

@RepositoryDefinition(domainClass = WorkoutEntity.class, idClass = Long.class)
public interface WorkoutRepository extends CrudRepository<WorkoutEntity, Long>{
	Optional<WorkoutEntity> findById(long id);
}
