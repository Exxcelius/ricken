package de.thb.ricken.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;

import de.thb.ricken.entity.ExerciseEntity;

@RepositoryDefinition(domainClass = ExerciseEntity.class, idClass = Long.class)
public interface ExerciseRepository extends CrudRepository<ExerciseEntity, Long>{
	Optional<ExerciseEntity> findById(long id);
}
