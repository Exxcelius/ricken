package de.thb.ricken.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;

import de.thb.ricken.entity.ExerciseFeatureUnitEntity;

@RepositoryDefinition(domainClass = ExerciseFeatureUnitEntity.class, idClass= Long.class)
public interface ExerciseFeatureUnitRepository extends CrudRepository<ExerciseFeatureUnitEntity, Long>{
	public Iterable<ExerciseFeatureUnitEntity> findAll();
	public Optional<ExerciseFeatureUnitEntity> findById(long id);
}
