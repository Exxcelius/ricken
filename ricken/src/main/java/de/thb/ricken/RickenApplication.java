package de.thb.ricken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RickenApplication {

  public static void main(String[] args) {
    SpringApplication.run(RickenApplication.class, args);
  }

}
